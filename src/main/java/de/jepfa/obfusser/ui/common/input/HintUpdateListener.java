package de.jepfa.obfusser.ui.common.input;

public interface HintUpdateListener {
    void onHintUpdated(int index);
}
