# Password Memorizer

A simple Android app to help to remember passwords. Instead of storing passwords like a common password manager, it stores patterns of passwords that helps to remember them. 

https://password-memorizer.jepfa.de/
